export interface HttpOption {
  url: string
  data?: any
  method?: string
}

export interface Response {
  totalSize: number | 0
  code: number
  msg: string
  data: any
}
