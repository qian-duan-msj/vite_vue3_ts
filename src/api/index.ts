import request from '@/api/request'

export function get(url: any, params: any = null, method: any = 'GET') {
  return request({
    url,
    method,
    params,
  })
}

export function getJoin(url: any, params: any, method: any = 'GET') {
  return request({
    url: url + '/' + params,
    method,
  })
}

export function post(url: any, data: any, method: any = 'POST') {
  return request({
    url,
    method,
    data,
  })
}

export function put(url: any, data: any, method: any = 'PUT') {
  return request({
    url,
    method,
    data,
  })
}

export function del(url: any, params: any, method: any = 'DELETE') {
  return request({
    url: url + '/' + params,
    method,
  })
}
