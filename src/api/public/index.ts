
import { get, getJoin, post, put, del } from '../index'

const url = import.meta.env.VITE_APP_WEB_COMMUNITY

// 资产账户查询
export function selectAll(data: any) {
  return post(url + '/statistics/data', data)
} 