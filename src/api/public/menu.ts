
import { get, post, put, del } from '../index'

// 分页查询权限
export function selectAll(data: any) {
  return post('/admin/adminpower/selectJiaoyiAdminPowerListPage', data)
}

// 权限新增
export function selectAdd(data: any) {
  return post('/admin/adminpower/instertJiaoyiAdminPower', data)
}

// 权限修改
export function selectUpdate(data: any) {
  return post('/admin/adminpower/updateJiaoyiAdminPower', data)
}

// 权限获取
export function selectAllPermissions(data: any = null) {
  return get('/admin/adminrolepower/selectJiaoyiAdminRolePowerUserId', data)
}

