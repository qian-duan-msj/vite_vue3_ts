
import { get, post } from '../index'
const url = import.meta.env.VITE_APP_WEB_EXCEL
// cate  数据类型 0-用户信息 1-账户变动 2-释放账户变动 3-真实当前订单
// 导出
export function selectExcel(data: any) {
  return get(url + '/api/export', data)
} 