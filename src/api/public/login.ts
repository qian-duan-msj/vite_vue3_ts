
import { get, getJoin, post, put, del } from '../index'

// 登录
export function login(data: any) {
  return post('/admin/adminUser/loginAdminUser', data)
}

// 后台用户信息查询
export function selectAll(data: any) {
  return post('/admin/adminUser/selectAdminUserPageList', data)
}

// 后台用户信息 添加
export function selectAdd(data: any) {
  return post('/admin/adminUser/instertJiaoyiAdminUser', data)
}

// 后台用户信息 修改
export function selectUpdate(data: any) {
  return post('/admin/adminUser/updateJiaoyiAdminUser', data)
}

// 后台用户信息 绑定角色回显
export function selectAllPermissions(data: any = null) {
  return get('/admin/adminuserpower/selectJiaoyiAdminUserPowerUserId', data)
}

// 后台用户信息 绑定角色 权限修改
export function selectAssignPermissions(data: any) {
  return post('/admin/adminuserpower/updateJiaoyiAdminUserPower', data)
}




// 后台用户信息 绑定角色回显
export function selectGetUserRoles(data: any) {
  return getJoin('/sys/user/getUserRoles', data)
}

// 后台用户信息 绑定角色
export function selectAssignRoles(data: any) {
  return post('/sys/user/assignRoles', data)
}

// 根据token查询权限树
export function selectAllPermissionsTree(data: any = null) {
  return get('/sys/permission/getPermTreeByToken', data)
}
