import Axios, { AxiosResponse, AxiosRequestConfig } from 'axios'
import qs from 'qs'
import useUserStore from '@/store/modules/user'

const userStore = useUserStore()
// const url = 'http://192.168.1.44:83'
// const url = 'http://192.168.1.2:9010'
// const url = import.meta.env.VITE_APP_WEB_URL
// console.log(import.meta.env.VITE_APP_WEB_URL);

export const baseURL: any = import.meta.env.VITE_APP_WEB_URL

export const CONTENT_TYPE = 'Content-Type'

export const FORM_URLENCODED = 'application/x-www-form-urlencoded; charset=UTF-8'

export const APPLICATION_JSON = 'application/json; charset=UTF-8'

export const TEXT_PLAIN = 'text/plain; charset=UTF-8'

const service = Axios.create({
  baseURL,
  timeout: 10 * 60 * 1000,
  withCredentials: false, // 跨域请求时发送cookie
})

function messView() {
  return (window as any)?.$message
}

service.interceptors.request.use(
  (config): AxiosRequestConfig<any> => {
    const endType: any = 2
    config.headers = {
      // endType,
      // language: "cn",
      // authorization: userStore.token,
      token: userStore.token,
      ...config.headers,
      // 'Content-Type': 'application/x-www-form-urlencoded',
      // 'Content-Type': 'application/json;charset=UTF-8'
    }
    return config
  },
  (error) => {
    return Promise.reject(error.response)
  }
)
service.interceptors.response.use(
  (response) => {
    const message = messView()
    const res = response.data
    const code = response.data.c

    let data = null
    if (userStore.token) {
      if (!res.d?.total) {
        data = res
      } else if (res.d) {
        data = res.d
      }
    } else {
      data = res
    }
    // const data = userStore.token ? res.d ? res.d : res : res
    if (code === '200') {
      return data
    } else if (code === '300') {
      userStore.logout()
      message?.error(res.m)
      return Promise.reject(res.m)
    } else {
      const errors = res.c + ':' + res.m
      message?.error(errors)
      return Promise.reject(errors)
    }
  },
  (error) => {
    const message = messView()
    if (import.meta.env.MODE === 'development') {
      message?.error(error.message)
      throw new Error(error.message.toString())
    }
    message?.error(error.message)
    return Promise.reject({ code: 500, msg: '服务器异常，请稍后重试…' })
  }
)

export default service
